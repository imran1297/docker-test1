# install of python packages
FROM ubuntu:20.04

#install node Packages
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y && \
    curl -sL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash - && \
    apt-get install -y nodejs && \
    apt install npm -y

#python packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3 \
    python3-pip \
    && \
    apt-get clean

# yarn package
RUN apt install curl wget -y && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -  && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -  && \
    apt update && \
    apt install yarn -y 

# java packages
RUN apt install default-jdk -y

# Go lang
RUN wget https://go.dev/dl/go1.17.7.linux-amd64.tar.gz 
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.7.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin
RUN echo $PATH && go version

# Nginx
RUN apt install nginx -y

# django
RUN apt install python3-django -y

# wordpress
### install dependecies
RUN apt install -y apache2  ghostscript  libapache2-mod-php  mysql-server php php-bcmath  php-curl  php-imagick php-intl php-json php-mbstring php-mysql php-xml  php-zip

RUN mkdir -p /srv/www && curl https://wordpress.org/latest.tar.gz | tar zx -C /srv/www  && chown www-data: /srv/www

# redis-server
RUN apt install redis-server -y && \
    service redis-server restart


